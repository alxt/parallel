package com.gitlab.alxt.parallel;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ParallelExecutor {

    private final ExecutorService executorService;
    private final Map<String, Lock> lockMap;

    public ParallelExecutor(int threadCount) {
        executorService = Executors.newFixedThreadPool(threadCount);
        lockMap = new ConcurrentHashMap<>();
    }

    public void run(ParallelTask task) {
        executorService.submit(() -> executeTask(task));
    }

    public boolean awaitTermination(long timeout, TimeUnit unit) throws InterruptedException {
        return executorService.awaitTermination(timeout, unit);
    }

    public List<Runnable> shutdownNow() {
        return executorService.shutdownNow();
    }

    private void executeTask(ParallelTask task) {
        Lock lock = lockMap.computeIfAbsent(task.getGroupName(), g -> new ReentrantLock());
        // Lock lock = lockMap.computeIfAbsent(task.getGroupName(), g -> new EmptyLock()); //!!! for "conflict" error
        lock.lock();
        try {
            task.run();
        } finally {
            lockMap.remove(task.getGroupName());
            lock.unlock();
        }
    }

    static class EmptyLock extends ReentrantLock {
        @Override
        public void lock() {
        }

        @Override
        public void unlock() {
        }
    }

}
