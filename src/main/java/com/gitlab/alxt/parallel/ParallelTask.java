package com.gitlab.alxt.parallel;

public interface ParallelTask extends Runnable {

    String getGroupName();

}
