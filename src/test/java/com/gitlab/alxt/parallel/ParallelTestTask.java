package com.gitlab.alxt.parallel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ParallelTestTask implements ParallelTask {

    private static final Map<String, ParallelTestTask> taskMap = new HashMap<>();
    private static final List<String> errors = new ArrayList<>();

    public final String groupName;
    public final int num;

    public ParallelTestTask(String groupName, int num) {
        this.groupName = groupName;
        this.num = num;
    }

    @Override
    public String getGroupName() {
        return groupName;
    }

    @Override
    public void run() {
        synchronized (taskMap) {
            if (taskMap.containsKey(groupName)) {
                errors.add("for group " + groupName + " conflict num " + num + " and " + taskMap.get(groupName).num);
            }
            taskMap.put(groupName, this);
            System.out.println("start " + groupName + ":" + num);
        }
        try {
            Thread.sleep(1000);
            System.out.println("stop " + groupName + ":" + num);
        } catch (InterruptedException e) {
            System.out.println("interrupt " + groupName + ":" + num);
        } finally {
            synchronized (taskMap) {
                ParallelTestTask task = taskMap.get(groupName);
                if (task == null) {
                    errors.add("group " + groupName + " was deleted by somebody");
                } else if (task != this) {
                    errors.add("for group " + groupName + " task " + num + " replaced by " + task.num);
                }
                taskMap.remove(groupName);
            }
        }
    }

    public static List<String> getErrors() {
        return errors;
    }
}
