package com.gitlab.alxt.parallel;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class ParallelExecutorTest {

    @Test
    public void parallelTest() throws InterruptedException {
        ParallelExecutor executor = new ParallelExecutor(10);
//        ParallelExecutor executor = new ParallelExecutor(1); //!!! For "No parallel execution" error
        executor.run(new ParallelTestTask("1", 1));
        executor.run(new ParallelTestTask("1", 2));
        executor.run(new ParallelTestTask("2", 1));
        executor.run(new ParallelTestTask("3", 1));
        executor.run(new ParallelTestTask("4", 1));
        Thread.sleep(1500);
        System.out.println("interrupt ALL");
        Assert.assertTrue(executor.shutdownNow().isEmpty(), "No parallel execution");
        Assert.assertTrue(executor.awaitTermination(100, TimeUnit.MILLISECONDS));
        Assert.assertTrue(ParallelTestTask.getErrors().isEmpty(), "\n" + ParallelTestTask.getErrors().stream().collect(Collectors.joining("\n")) + "\n");
    }

}
